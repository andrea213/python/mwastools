mwastools
=========

mwastools provides an integrated framework to perform metabolome-wide association studies.

Quick Start
-----------

With ``poetry`` installed, you can run the code in this project as follows:

.. code-block:: bash

    # go to your root project directory
    $ cd mwastools

    # install the package and its core dependencies in a virtual environment
    $ poetry install --no-dev

    # test your code in the REPL
    $ poetry run python
    >>> import mwastools

Documentation
-------------

`Read the docs <https://andrea213.gitlab.io/python/mwastools>`_!