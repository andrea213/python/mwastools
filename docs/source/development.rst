Development
===========


Pre-requisites
--------------

poetry
^^^^^^

This project is based on the ``poetry`` tool for dependency management and packaging in Python. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you. To start working on this project, make sure that ``poetry`` is installed on your system.

.. seealso::

    https://python-poetry.org/docs/#installation


(Optional) ``poetry`` creates and manages virtual environments for you automatically in a centralised cache directory. If you would rather have these virtual environments created and expected within the root directory of the project, set:

.. code-block:: bash

    # configure poetry for in-project virtualenvs
    $ poetry config virtualenvs.in-project true

nox
^^^

This project defines a number of pre-configured CI/CD routines in ``noxfile.py`` - which are automatically run on commit by the ``gitlab`` pipeline defined in ``.gitlab-ci.yml``.

To test these routines locally while developing your project, you first need to have installed `nox-poetry <https://github.com/cjolowicz/nox-poetry>`_ on your system:

.. code-block:: bash

    # install nox-poetry from the Python Package Index
    $ pip install nox-poetry

You can then manually run any of the sessions defined in ``noxfile.py`` as:

.. code-block:: bash

    # run a nox session
    $ python -m nox -rs <session_name>

.. seealso::

    https://nox.thea.codes/en/stable/

git hooks
^^^^^^^^^

This project ships with a number of git-hooks, to enforce baseline quality standards for code development.

To take advantage of these hooks, you first need to have installed the `pre-commit <https://pre-commit.com/>`_ package on your system:

.. code-block:: bash

    # install pre-commit from the Python Package Index
    $ pip install pre-commit

Then install the pre-commit hooks for this project defined in the ``.pre-commit-config.yaml`` file:

.. code-block:: bash

    # go to your root project directory
    $ cd {{ cookiecutter.project_slug }}

    # install pre-defined hooks
    $ python -m pre_commit install

Finally, run the hooks once against all files:

.. code-block:: bash

    $ python -m pre_commit run --all-files

Hooks defined in ``.pre-commit-config.yaml`` will now automatically hook to all of your git commits.

Coding
------

When working on adding new features to your codebase, you will want to develop and test your code interactively:

.. code-block:: bash

    # go to your root project directory
    $ cd {{ cookiecutter.project_slug }}

    # install the package in editable mode (including --dev dependencies)
    $ poetry install

    # ... work on the code ... (changes will be reflected live)

    # test features in REPL
    $ poetry run python

Once your package is installed locally, you can also run arbitrary commands against your codebase within your local virtual environment. For example:

.. code-block:: bash

    # execute an arbitrary command from within the virtualenv
    $ poetry run <cmd>

Documentation
-------------

You can build the documentation through the *docs* ``nox`` session:

.. code-block:: bash

    # run documentation nox session
    $ python -m nox -rs docs

Testing
-------

You can run pytests through the *tests* ``nox`` session:

.. code-block:: bash

    # run pytests nox session
    $ python -m nox -rs tests
