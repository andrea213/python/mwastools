Quick Start
===========

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sodales augue efficitur elit pretium vehicula. Nullam et magna sodales, semper ipsum id, accumsan libero. Nullam non velit sed lorem elementum tristique. Nullam pretium lectus nec imperdiet lacinia. Aliquam ut lorem bibendum, ultricies nisi non, imperdiet neque. Duis ut iaculis tortor. Mauris tincidunt elit at purus auctor aliquet. Sed hendrerit varius nisi quis eleifend. Vestibulum et ipsum pellentesque, dictum elit eu, malesuada augue. Fusce eleifend tristique dolor, accumsan auctor eros. Vestibulum congue orci purus, id faucibus erat rhoncus eu. Proin at augue sed velit posuere efficitur sit amet et massa. Nunc eros mauris, convallis in rhoncus eu, vulputate eget erat.

Pre-requisites
--------------

This project is based on the ``poetry`` tool for dependency management and packaging in Python. It allows to declare the libraries the project depends on and it will manage (install/update) them for you. Make sure that ``poetry`` is installed on your system before moving on to the next step.

.. seealso::

    https://python-poetry.org/docs/#installation


(Optional) ``poetry`` creates and manages virtual environments for you automatically in a centralised cache directory. If you would rather have these virtual environments created and expected within the root directory of the project, set:

.. code-block:: bash

    $ poetry config virtualenvs.in-project true


Minimal Working Example
-----------------------

With ``poetry`` installed, you can run the code in this project as follows:

.. code-block:: bash

    # go to your root project directory
    $ cd {{ cookiecutter.project_slug }}

    # install the package and its core dependencies in a virtual environment
    $ poetry install --no-dev

    # test your code in the REPL
    $ poetry run python
    >>> import {{ cookiecutter.package_name }}
