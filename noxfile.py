"""Defines session configurations for ``nox``.

Nox is a command-line tool that automates testing in multiple Python
environments.

Examples
--------
Globally install nox-poetry from the Python Package Index:

.. code-block:: bash

    $ pip install nox-poetry

and run these nox sessions with:

.. code-block:: bash

    $ python -m nox [-r] [--sessions <name>-<x.y>]
"""

import textwrap

try:
    from nox_poetry import Session, session

except ImportError:

    message = """\
    \n❌ Faiiiled to import the 'nox-poetry' package.\n
    Please install it using the following command:\n
    \t$ python -m pip install nox-poetry\n"""

    raise SystemExit(textwrap.dedent(message))

_PACKAGE_NAME = "mwastools"
_CODE_LOCATIONS = "src", "tests", "noxfile.py"


@session(python="3.8")  # type: ignore
def black(session: Session) -> None:
    """Run black formatting introspection on the codebase."""

    args = session.posargs or _CODE_LOCATIONS

    session.install("black")

    session.run("black", *args, "--check")


@session(python="3.8")  # type: ignore
def lint_quality(session: Session) -> None:
    """Run quality-related flake8 linting on the codebase."""

    args = session.posargs or _CODE_LOCATIONS

    session.install(
        "flake8",
        "flake8-annotations",
        "flake8-bugbear",
        "flake8-docstrings",
        "flake8-import-order",
    )

    session.run("flake8", *args)


@session(python="3.8")  # type: ignore
def lint_security(session: Session) -> None:
    """Run security-related flake8 linting on the codebase."""

    args = session.posargs or _CODE_LOCATIONS

    session.install(
        "flake8",
        "flake8-bandit",
        "dlint",
    )

    session.run("flake8", *args)


@session(python="3.8")  # type: ignore
def typing(session: Session) -> None:
    """Run mypy type-checking on the codebase."""

    args = session.posargs or _CODE_LOCATIONS

    session.install("mypy")

    session.run("mypy", *args)


@session(python="3.8")  # type: ignore
def tests(session: Session) -> None:
    """Run the pytest test suite (optionally) with coverage."""

    args = session.posargs or ["-v", "--cov"]

    session.install(".")
    session.install("pytest", "coverage", "pytest-cov")

    session.run("pytest", *args)


@session(python="3.8")  # type: ignore
def xdoctests(session: Session) -> None:
    """Run the docstring examples with xdoctest."""

    args = session.posargs or ["all"]

    session.install(".")
    session.install("xdoctest", "pygments")

    session.run("python", "-m", "xdoctest", _PACKAGE_NAME, *args)


@session(python="3.8")  # type: ignore
def safety(session: Session) -> None:
    """Run Safety checks on project dependencies."""

    path = session.poetry.export_requirements()

    session.install("safety")

    session.run("safety", "check", f"--file={path}", "--full-report")


@session(python="3.8")  # type: ignore
def docs(session: Session) -> None:
    """Build the documentation."""

    session.install(".")
    session.install(
        "sphinx",
        "sphinx-book-theme",
        "sphinx-copybutton",
        "sphinx-panels",
        "sphinx-autodoc-typehints",
    )

    session.run("sphinx-apidoc", "-f", "-o", "docs/source/", "src/")
    session.run("sphinx-build", "-a", "docs/source/", "docs/build/html")
