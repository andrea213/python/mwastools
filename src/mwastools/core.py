"""Define core package functionalities."""

from typing import Literal, NamedTuple

import numpy as np
import pandas as pd
from scipy.stats import boxcox, norm
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

NormalisationMethod = Literal["irnt", "log10", "box-cox"]


class PCAresults(NamedTuple):
    """Aggregate Principal Component Analysis results.

    Parameters
    ----------
    scores : pd.Dataframe
        PCA scores.
    loadings: pd.Dataframe
        PCA loadings.
    vars:
        PCA variances across the principal components.
    """

    scores: pd.DataFrame
    loadings: pd.DataFrame
    vars: pd.DataFrame


def _replace_inf_values(data: pd.DataFrame) -> pd.DataFrame:
    """
    Replace +/- Inf values by the max/min of the corresponding feature.

    Parameters
    ----------
    data : pd.DataFrame

    Returns
    -------
    pd.DataFrame
        Input DataFrame with replaced Inf values.
    """

    data = data.where(
        data != np.PINF, data.replace(np.PINF, np.nan).max(), axis=1
    )

    data = data.where(
        data != np.NINF, data.replace(np.NINF, np.nan).min(), axis=1
    )

    return data


def _get_mean_values(data: pd.DataFrame, metadata: pd.DataFrame) -> pd.Series:
    """
    Get mean intenisty value of metabolomic features.

    Parameters
    ----------
    data : pd.DataFrame
        Metabolomic data with samples in the rows and features in the columns.
    metadata : pd.DataFrame
        Sample metadata with samples in the rows and features in the columns.

    Returns
    -------
    pd.Series
        Mean intensity values.
    """

    # Get qc data (if available)
    qc_data = data[metadata["sample_type"] == 1]

    # Calculate mean intensity values across qc samples (if available); else
    # use the actual samples
    if qc_data.empty:
        mean_vals = data.mean()
    else:
        mean_vals = qc_data.mean()

    return mean_vals


def _check_numeric(df: pd.DataFrame) -> bool:
    """
    Check that all values in a given DataFrame are numeric (int or float).

    Parameters
    ----------
    data : pd.DataFrame

    Returns
    -------
    bool
        Indicates whether all values in data are numeric or not.
    """

    res = df.apply(
        lambda x: pd.to_numeric(x[x.notnull()], errors="coerce")
        .notnull()
        .all()
    ).all()  # type: bool

    return res


def _check_input(data: pd.DataFrame, metadata: pd.DataFrame) -> None:
    """
    Check that input DataFrames have the right format.

    Parameters
    ----------
    data : pd.DataFrame
        Metabolomic data with samples in the rows and features in the columns.
    metadata : pd.DataFrame
        Sample metadata with samples in the rows and features in the columns.
    """

    # Check that both data and metadata are DataFrames
    if not isinstance(data, pd.DataFrame):
        raise TypeError("Data must be a pd.DataFrame")
    if not isinstance(metadata, pd.DataFrame):
        raise TypeError("Metadata must be a pd.DataFrame")

    # Check that both data and metadata contain only numeric values
    if not _check_numeric(data):
        raise TypeError("All values in data must be numeric")
    if not _check_numeric(metadata):
        raise TypeError("All values in metadata must be numeric")

    # Check number of samples
    if data.shape[0] != metadata.shape[0]:
        raise ValueError("Data and metadata have incompatible dimensions")

    # Check number sample labels
    if not set(data.index) == set(metadata.index):
        raise ValueError("Data and metadata have incompatible sample labels")

    # Make sure that the samples have the same order in both data and metadata
    metadata = metadata.loc[data.index]

    # If `sample_type` column not existent, assume that there are no QC samples
    if "sample_type" not in metadata.columns:
        metadata["sample_type"] = 0
    else:
        if not set(metadata["sample_type"]).issubset({0, 1}):
            raise ValueError(
                "Incorrect sample_type values: should be either "
                "0 (sample) or 1 (qc)"
            )


class MetaboLoader:
    """
    Class for loading and analysing metabolomics data.

    Parameters
    ----------
    data : pd.DataFrame
        Metabolomic data (e.g. NMR peak intensities or metabolite
        concentrations). The columns of the DataFrame correspond to the
        metabolomic features and the rows to the samples. Column and row names
        the metabolite IDs (e.g. chemical shifts for NMR data) and sample
        IDs, respectively.
    metadata : pd.DataFrame
        Sample metadata (e.g. age, gender, disease status). All features
        must be numeric. The columns of the DataFrame correspond to the
        sample features and the rows to the samples. Column and row names
        must contain the feature IDs and the sample IDs, respectively. If
        quality control (QC) samples are available, the DataFrame should
        contain a 'sample_type' column indicating whether samples are
        clinical samples (0) or qc samples (1). For samples without clinical
        data (e.g. qc samples), np.nan values must be used.
    """

    def __init__(self, data: pd.DataFrame, metadata: pd.DataFrame) -> None:
        """Initialise class."""

        self.data = data
        self.metadata = metadata

        _check_input(self.data, self.metadata)

    def get_cv(self) -> pd.DataFrame:
        """
        Calculate coefficient of variation.

        The coefficient of variantion (cv) is calculated as the ratio
        of standard deviation to mean. The cv is calculated across all
        features in data using only the quality control (qc) samples.

        Returns
        -------
        pd.DataFrame
            Features in data and associated coefficients of variation and mean
            intensity across the qc samples.

        Raises
        ------
        ValueError
            If number of qc samples is insufficient.
        """

        # Get qc data
        qc_data = self.data[self.metadata["sample_type"] == 1]

        if qc_data.shape[0] < 2:
            raise ValueError("At least 2 samples are need for CV calculation")

        mean_values = qc_data.mean(axis=0)
        cv_values = qc_data.std(axis=0) / mean_values

        cv_df = pd.DataFrame(
            {
                "feature": qc_data.columns,
                "cv": cv_values,
                "mean_intensity": mean_values,
            }
        )
        cv_df = cv_df.reset_index(drop=True)

        return cv_df

    def pca(self, n: int = 10, scale: bool = True) -> PCAresults:
        """
        Perform principal component analysis.

        Before applying pca, missing values in the metabolomic data are
        replaced with the average of the corresponding feature and features
        are optionally standardised (zero mean and unit variance).

        Parameters
        ----------
        n : int
            Number of principal components.
        scale : bool
            Indicates whether the data will be scaled (mean = 0, sd = 1) or
            not.

        Returns
        -------
        PCAresults
            Principal Component Analysis results.
        """

        # Prepare data
        data = self.data
        mean_vals = _get_mean_values(data=data, metadata=self.metadata)
        data = data.fillna(mean_vals)
        if scale:
            data = StandardScaler().fit_transform(data)

        # PCA model
        pca = PCA(n_components=n)
        pca_scores = pca.fit_transform(data)
        pca_loadings = pca.components_.T
        pca_var = pca.explained_variance_ratio_

        # Collect model outputs of interest
        pc_cols = [f"PC_{num}" for num in range(1, n + 1)]

        pca_scores_df = pd.DataFrame(
            data=pca_scores, columns=pc_cols, index=self.metadata.index
        )
        pca_scores_df["sample_type"] = self.metadata["sample_type"]

        pca_loadings_df = pd.DataFrame(
            data=pca_loadings, columns=pc_cols, index=self.data.columns
        )
        pca_loadings_df["mean_intensity"] = mean_vals

        pca_variance_df = pd.DataFrame({"pc": pc_cols, "var": pca_var})

        return PCAresults(
            scores=pca_scores_df,
            loadings=pca_loadings_df,
            vars=pca_variance_df,
        )

    def normalise(self, method: NormalisationMethod = "irnt") -> pd.DataFrame:
        """
        Transform metabolomic features for better normal distribution.

        Three normalisation methods can be used: 1) inverse rank normal
        transformation; 2) log10 transformation; and 3) box-cox transformation.

        Parameters
        ----------
        method : {"irnt", "log10", "box-cox"}
            Normalisation method.

        Returns
        -------
        pd.DataFrame
            Normalised data.
        """

        # Prepare data
        data = self.data
        mean_vals = _get_mean_values(data=data, metadata=self.metadata)
        data = data.fillna(mean_vals)

        if method == "irnt":
            data = (1 + data.rank()) / (1 + len(data) - data.isna().sum())
            data = data.apply(norm.ppf)

        elif method == "log10":
            data = np.log10(data)

        elif method == "box-cox":
            data = data.apply(lambda x: boxcox(x)[0], axis=0)

        else:
            raise ValueError(
                f"Invalid method. Use one of: "
                f"{NormalisationMethod.__args__}"
            )

        return data

    def stocsy(
        self, driver_feature: str, method: str = "pearson"
    ) -> pd.DataFrame:
        """
        Compute statistical total correlation spectroscopy.

        Calcualate the covariance and correlation between a metabolomic feature
        of interest (typically an NMR signal) and all the metabolomic features.

        Parameters
        ----------
        driver_feature : string
            Label of the metabolomic feature that will be used as the driver
            signal.
        method : string
            Correlation method (spearman, pearson, kendall)

        Returns
        -------
        pd.DataFrame
            Features in data and their associated covariance and correlation
            with the driver feature. The 'mean_intensity' column corresponds to
            the mean intensity of each feature across the qc samples. If qc
            samples are not available the mean intensity is calculated across
            the actual samples.
        """

        # Check that the driver feature is included in data
        if driver_feature not in self.data.columns:
            raise ValueError("Driver feature is not included in data")

        # Split data in actual data and qc data
        data = self.data
        mean_vals = _get_mean_values(data=data, metadata=self.metadata)
        data = data.fillna(mean_vals)
        actual_data = data[self.metadata["sample_type"] == 0]

        # Compute covariance and correlations
        cov = list(actual_data.cov().loc[driver_feature])
        corr = list(
            actual_data.corrwith(actual_data[driver_feature], method=method)
        )

        # Prepare output
        stocsy_df = pd.DataFrame(
            {
                "driver_feature": driver_feature,
                "target_feature": actual_data.columns,
                "cov": cov,
                "corr": corr,
                "mean_intensity": mean_vals,
            }
        )

        return stocsy_df
