"""Pytest configuration file."""

import numpy as np
import pandas as pd
import pytest

MOCK_SAMPLE_IDS = ["P1", "P2", "Q1", "Q2"]


@pytest.fixture  # type: ignore
def mock_data() -> pd.DataFrame:
    """Mock data dataframe."""

    return pd.DataFrame(
        {
            "ppm_1": [0.5, 1, 0.7, 0.8],
            "ppm_2": [0.6, 1.5, 0.8, 0.8],
        },
        index=MOCK_SAMPLE_IDS,
    )


@pytest.fixture  # type: ignore
def mock_metadata() -> pd.DataFrame:
    """Mock metadata dataframe."""

    return pd.DataFrame(
        {
            "age": [40, 50, np.nan, np.nan],
            "sex": [1, 2, np.nan, np.nan],
            "sample_type": [0, 0, 1, 1],
        },
        index=MOCK_SAMPLE_IDS,
    )
