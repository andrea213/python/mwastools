"""Tests :func:get_cv method of the :cls:mwastools.core.MetaboLoader class."""

import numpy as np
import pandas as pd
import pytest

from mwastools.core import MetaboLoader


def test_algorithm(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Give correct numeric result."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    observed_val = np.round(loader.get_cv()["cv"][0], 2)

    expected_val = np.round(
        np.std([0.7, 0.8], ddof=1) / np.mean(([0.7, 0.8])), 2
    )

    assert observed_val == expected_val


def test_not_enough_samples(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Raise ValueError if not enough qc samples."""

    mock_data, mock_metadata = mock_data.iloc[1:2], mock_metadata.iloc[1:2]

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    with pytest.raises(ValueError):

        loader.get_cv()
