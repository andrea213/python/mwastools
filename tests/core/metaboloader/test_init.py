"""Tests initialisation of the :cls:mwastools.core.MetaboLoader class."""

import pandas as pd
import pytest

from mwastools.core import MetaboLoader


def test_init_result(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Instantiate MetaboLoader object."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    assert isinstance(loader, MetaboLoader)


def test_set_data(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Data attribute is set as expected."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    pd.testing.assert_frame_equal(loader.data, mock_data)


def test_set_metadata(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Data attribute is set as expected."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    pd.testing.assert_frame_equal(loader.metadata, mock_metadata)


def test_invalid_data_type(mock_metadata: pd.DataFrame) -> None:
    """Raise TypeError if not passing data of DataFrame type."""

    with pytest.raises(TypeError):
        MetaboLoader(data=1, metadata=mock_metadata)


def test_invalid_metadata_type(mock_data: pd.DataFrame) -> None:
    """Raise TypeError if not passing metadata of DataFrame type."""

    with pytest.raises(TypeError):
        MetaboLoader(data=mock_data, metadata=1)


def test_mismatched_dimensions(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Raise ValueError if data and metadata have incompatible dimensions."""

    mock_data = mock_data.iloc[1:2]

    with pytest.raises(ValueError):
        MetaboLoader(data=mock_data, metadata=mock_metadata)


def test_mismatched_sample_ids(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Raise ValueError if data and metadata have mismatched sample IDs."""

    mock_metadata = mock_metadata.reset_index(drop=True)

    with pytest.raises(ValueError):
        MetaboLoader(data=mock_data, metadata=mock_metadata)
