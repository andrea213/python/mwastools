"""Tests :func:normalise method of the :cls:mwastools.core.MetaboLoader."""

import pandas as pd
import pytest

from mwastools.core import MetaboLoader


def test_irnt(mock_data: pd.DataFrame, mock_metadata: pd.DataFrame) -> None:
    """Give correct dimensions."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    res = loader.normalise(method="irnt")

    assert res.shape == loader.data.shape


def test_log10(mock_data: pd.DataFrame, mock_metadata: pd.DataFrame) -> None:
    """Give correct dimensions."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    res = loader.normalise(method="log10")

    assert res.shape == loader.data.shape


def test_box_cox(mock_data: pd.DataFrame, mock_metadata: pd.DataFrame) -> None:
    """Give correct dimensions."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    res = loader.normalise(method="box-cox")

    assert res.shape == loader.data.shape


def test_incorrect_method(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Raise ValueError if the method is invalid."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    with pytest.raises(ValueError):

        loader.normalise(method="method_random")  # type: ignore
