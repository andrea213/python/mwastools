"""Tests :func:pca method of the :cls:mwastools.core.MetaboLoader class."""

import pandas as pd

from mwastools.core import MetaboLoader


def test_scores_shape(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Give correct number of columns."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    res = loader.pca(n=2)

    assert res.scores.shape[1] == 3


def test_scores_index(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Give correct index."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    res = loader.pca(n=2)

    assert set(res.scores.index) == set(loader.data.index)


def test_loadings_shape(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Give correct number of columns."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    res = loader.pca(n=2)

    assert res.loadings.shape[1] == 3


def test_loadings_index(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Give correct index."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    res = loader.pca(n=2)

    assert set(res.loadings.index) == set(loader.data.columns)
