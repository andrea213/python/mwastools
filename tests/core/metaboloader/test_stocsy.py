"""Tests :func:stocsy method of the :cls:mwastools.core.MetaboLoader class."""

import pandas as pd
import pytest

from mwastools.core import MetaboLoader


def test_output_shape(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Give correct number of rows."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    res = loader.stocsy(driver_feature="ppm_1")

    assert res.shape[0] == 2


def test_incorrect_driver_feature(
    mock_data: pd.DataFrame, mock_metadata: pd.DataFrame
) -> None:
    """Raise ValueError if the driver feature is not included in data."""

    loader = MetaboLoader(data=mock_data, metadata=mock_metadata)

    with pytest.raises(ValueError):

        loader.stocsy(driver_feature="ppm_random")
