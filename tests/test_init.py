"""Tests components of the :mod:mwastools.__init__ module."""

from mwastools import __version__, package_version


def test_version() -> None:
    """Package has expected version."""

    assert __version__ == package_version()
